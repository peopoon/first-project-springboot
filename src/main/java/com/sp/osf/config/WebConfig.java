package com.sp.osf.config;
import java.util.concurrent.TimeUnit;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.CacheControl;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

@Configuration
public class WebConfig extends WebMvcConfigurationSupport {
	
	@Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/img/**").addResourceLocations("/resources/upload/").
        setCacheControl(CacheControl.maxAge(2, TimeUnit.HOURS).cachePublic());
        
        registry.addResourceHandler("/video/**").addResourceLocations("/resources/video/").
        setCacheControl(CacheControl.maxAge(2, TimeUnit.HOURS).cachePublic());
    }
    
    
}