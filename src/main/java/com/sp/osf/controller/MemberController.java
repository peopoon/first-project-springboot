package com.sp.osf.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sp.osf.service.MemberService;
import com.sp.osf.vo.MemberVO;

import lombok.extern.slf4j.Slf4j;

@CrossOrigin("*")
@RestController
@Slf4j
public class MemberController {
	@Resource
	private MemberService ms;

	// 메일 인증 누르면
	@GetMapping("/member/joinConfirm")
	public String emailConfirm(MemberVO member) {
		ms.joinUpdate(member);
//		log.info("member=>{}", member);
//		log.info("model=>{}", model);
		return "member.getOmId님 회원가입을 축하합니다. 이제 로그인이 가능합니다";
	}

	//멤버 리스트
	@GetMapping("/members")
	public List<MemberVO> selectMemberList(MemberVO member) {
		return ms.selectMemberList(member);
	}

	// 아이디로 멤버 가져오기
	@GetMapping("/member/{omId}")
	public MemberVO findMemberById(@PathVariable("omId") String omId) {
		log.info("omId=>{}", omId);
		return ms.selectMemberById(omId);
	}

	// 아이오닉에는 @RequestBody만 있다
	//로그인
	@PostMapping("/login")
	public @ResponseBody MemberVO doLogin(@RequestBody MemberVO member) {
		log.info("member=>{}", member);
		return ms.selectMemberByIdAndPwd(member);
	}

	/*
	 * 아이오닉프로젝트는
	 * @ResponseBody
	 * @RequestBody 빼준다
	 회원가입              */
	@PostMapping("/member")
	public @ResponseBody Integer addMember(@RequestBody MemberVO member) {
		log.info("{}", member);
		return ms.insertMember(member);
	}
	
	@PostMapping("/member/modi")
	public Integer modifuMember(MemberVO member) {
		return ms.updateMember(member);
	}

	@PostMapping("/test")
	public @ResponseBody Integer testMember(@RequestBody MemberVO member) {
		log.info("member=>{}", member);
		return 1;
	}

	// 계정 삭제
	@DeleteMapping("/member/{omNo}")
	public Integer deleteMember(@PathVariable("omNo") Integer omNo) {
		log.info("omNo=>{}", omNo);
		return ms.deleteMember(omNo);
	}
	
	// 아이디 찾기
	@PostMapping("member/idfind")
	public @ResponseBody MemberVO idFind(@RequestBody MemberVO member) {
		log.info("member=>{}", member);
		return ms.selectIdFind(member);
	}

}
