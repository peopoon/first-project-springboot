//package com.sp.osf.controller;
//
//import java.util.List;
//
//import javax.annotation.Resource;
//
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//
//import com.sp.osf.service.BoardService;
//import com.sp.osf.vo.BoardVO;
//import com.sp.osf.vo.CommentsVO;
//import com.sp.osf.vo.PageDTO;
//
//@RestController
//public class BoardController {
//  
//	@Resource
//	private BoardService boardService;
//	
//	@RequestMapping(value="/list", method=RequestMethod.GET)
//	public String list(Model model, @RequestParam(value="pageNum", defaultValue="1") int pageNum) {
//		PageDTO page = new PageDTO();
//		page.setPageSize(10);
//		page.setPageBlock(3);
//		page.setPageNum(pageNum);
//		page.setCount(boardService.getCount());
//		
//		List<BoardVO> list = boardService.list(page);
//		model.addAttribute("list", list);
//		model.addAttribute("page", page); 
//		return "board/list";
//	}
//	
//	@RequestMapping(value="/write", method=RequestMethod.GET)
//	public String write() {
//		return "board/write";
//	}
//	
//	@RequestMapping(value="/view", method=RequestMethod.GET)
//	public String view(Model model, @RequestParam int no) {
//		BoardVO boardVO = new BoardVO();
//		boardVO = boardService.view(no);
//		List<CommentsVO> commentsList = boardService.listComments(no);
//		model.addAttribute("comments", commentsList);
//		model.addAttribute("board", boardVO);
//		return "board/view";
//	}
//	
//	
//	
//}
