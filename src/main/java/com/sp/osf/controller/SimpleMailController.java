package com.sp.osf.controller;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class SimpleMailController {

	@Autowired
	private JavaMailSender sender;
	
    @GetMapping("/views/web")
    public String goPage() {
        return "web";
    }
    
	
	// 텍스트만 있는 메일 보내기	
	// http://localhost:88/sendMail
    @RequestMapping("/sendMail")
	public String sendMail() throws MessagingException {
		MimeMessage message = sender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message);
		try {
			helper.setTo("litolf09@gmail.com");
			helper.setSubject("메일 제목 ");
			helper.setText("Greetings : 메일 내용 )");
		} catch (javax.mail.MessagingException e) {
			e.printStackTrace();
		}
		sender.send(message);
		
		return "Mail Sent Success!  or   메일을 보냈습니다";
	}
    
    
    

	
}
