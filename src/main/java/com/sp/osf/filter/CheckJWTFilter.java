package com.sp.osf.filter;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.springframework.web.filter.GenericFilterBean;

import com.sp.osf.auth.MakeJWT;
import com.sp.osf.vo.MemberVO;

import lombok.extern.slf4j.Slf4j;

//@Component
@Slf4j
public class CheckJWTFilter extends GenericFilterBean {
	@Resource
	private MakeJWT mjwt;
	private static final String[] EXCLUDE_URI = { "login", "member" };

	public boolean checkUri(String uri) {
		for (String excludeUri : EXCLUDE_URI) {
			if (uri.indexOf(excludeUri) != -1) {
				return false;
			}
		}
		return true;
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		String method = req.getMethod();
		String uri = req.getRequestURI();
		if (!method.equals("OPTIONS") && !req.getRequestURI().equals("/login")
				&& !req.getRequestURI().equals("/member")  && !req.getRequestURI().equals("/member/{omId}")
				
				) {
			String omId = req.getHeader("X-AUTH-ID");
			String token = req.getHeader("X-AUTH-TOKEN");

			try {
				MemberVO member = new MemberVO();
				member.setOmId(omId);
				mjwt.checkJWT(token, member);
			} catch (Exception e) {
				log.error("JWT error => {}", e);
				throw new ServletException("토큰키가 올바르지 않습니다");
			}

		}
		chain.doFilter(request, response);
	}

}
