package com.sp.osf.vo;

import org.apache.ibatis.type.Alias;
import org.springframework.web.multipart.MultipartFile;

import lombok.Data;
@Alias("member")
@Data
public class MemberVO {
	private Integer omNo;
	private String omDevice;
	private String omId;
	private String omNick;
	private String omPass;
	private String omMail;
	private String omProfile;
	
	private MultipartFile omProfileFile;
	
	private String omPhone;
	private String omBirth;
	private String omTrans;
	private String omToken;
	private String omZipcode;
	private String omAddr1;
	private String omAddr2;
	private String omInfo;
	
	// 회원가입시 메일인증을 위한  임시 인증키
	private String authkey;
	
	// 회원가입시 디폴트 0  메일인증 1
	private Integer authstatus;
	
}
