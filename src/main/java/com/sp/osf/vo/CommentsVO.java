package com.sp.osf.vo;

import lombok.Data;

@Data
public class CommentsVO {
	private int no;
	private int bNo;
	private String id;
	private String content;
	private String cDate;
}
