package com.sp.osf.vo;

import lombok.Data;

@Data
public class BoardVO {
	int no;
	String id;
	String title;
	String content;
	int gNo;
	int step;
	int indent;
	int count;
	String bDate;
}
