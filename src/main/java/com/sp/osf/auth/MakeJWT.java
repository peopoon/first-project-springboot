package com.sp.osf.auth;

import java.util.Calendar;
import java.util.Date;

import org.springframework.stereotype.Component;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.sp.osf.vo.MemberVO;

import lombok.extern.slf4j.Slf4j;
@Component
@Slf4j
public class MakeJWT {
	private static final Date ISSUE_DATE;
	private static final Date EXPIRE_DATE;
	private static final String SALT ="osf";

	static {
		Calendar calendar = Calendar.getInstance();
		ISSUE_DATE = calendar.getTime(); // 현재시간 생성일자
		calendar.add(Calendar.DATE, 10);
		EXPIRE_DATE = calendar.getTime(); // jwt 만료시간
	}

	public static String makeJWT(MemberVO member) {
//		log.info("JWT MEMBER =>{}",member);
		String jwt = JWT
				.create()
				.withIssuer(member.getOmId())
				.withIssuedAt(ISSUE_DATE)
				.withExpiresAt(EXPIRE_DATE)
				.sign(Algorithm.HMAC256(SALT));
		log.info("jwt => {}", jwt);
		return jwt;
	}
	
	public void checkJWT(String token, MemberVO member) {
		JWTVerifier verifier = JWT
				.require(Algorithm.HMAC256(SALT))
				.withIssuer(member.getOmId())
				.build();
		DecodedJWT decode = verifier.verify(token);
		log.info("decode => {}", decode);
		log.info("issuer => {} " , decode.getIssuer());
		log.info("issue date =>{} " , decode.getIssuedAt());
		log.info("expire date =>{}", decode.getExpiresAt());
	}
	
	public static void main(String[] args) {
		MakeJWT mjwt = new MakeJWT();
		MemberVO member = new MemberVO();
		
		member.setOmId("hoome");
		String jwt = mjwt.makeJWT(member);
		log.info("JWTJWT ++++>{}",jwt);
//		member.setOmId("hoome1");
//		mjwt.checkJWT(jwt, member);
	}

	
}
