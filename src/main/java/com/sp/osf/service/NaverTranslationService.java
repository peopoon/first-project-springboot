package com.sp.osf.service;

import java.util.List;
import java.util.Map;

public interface NaverTranslationService {
	public List<Map<String, Object>> selectTranslationHisList();
	public Map<String, Object> selectTranslationHisOne(Map<String, String> param);
	public Integer insertTranslationHis(Map<String, String> param);
	public Integer updateTranslationHis(Map<String, Object> rMap);

}
