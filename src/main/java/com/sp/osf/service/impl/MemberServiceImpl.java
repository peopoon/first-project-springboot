package com.sp.osf.service.impl;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.util.List;

import javax.annotation.Resource;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.sp.osf.auth.MakeJWT;
import com.sp.osf.common.MailUtils;
import com.sp.osf.common.TempKey;
import com.sp.osf.mapper.MemberMapper;
import com.sp.osf.service.MemberService;
import com.sp.osf.vo.MemberVO;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class MemberServiceImpl implements MemberService {

	@Resource
	private MemberMapper mm;

	private final String BASE = "D:\\study\\workspace\\osf\\src\\main\\webapp\\resources\\upload\\";
	
	@Autowired
	private JavaMailSender sender;
	
	@Override
	public List<MemberVO> selectMemberList(MemberVO member) {
		return mm.selectMemberList(member);
	}

	@Override
	public MemberVO selectMember(int omNo) {
		return mm.selectMember(omNo);
	}
	
	//회원가입
	@Override
	public int insertMember(MemberVO member) {
		
		// 메일 인증키 
		String authkey = new TempKey().getKey(50, false);
		member.setAuthkey(authkey);
		
		// 비밀번호 SHA
//		member.setOmPass(SHAEncoder.encode(member.getOmPass()));   // SHAEncoder 클래스를 사용한 방법
		member.setOmPass(DigestUtils.sha256Hex(member.getOmPass()));  // 라이브러리 사용한 방법
		
		// 메일보내기
		MimeMessage message = sender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message);
		try {
			helper.setTo(member.getOmMail());
			helper.setSubject("[페이스톡톡]" +member.getOmId()+"님 회원가입 이메일 인증");
			helper.setText(new StringBuffer().append("<h1>[이메일 인증]</h1>")
					.append("<p>아래 링크를 클릭하시면 이메일 인증이 완료됩니다.</p>")
					.append("<a href='http://localhost:88/member/joinConfirm?omId=")
					.append(member.getOmId())
					.append("&email=")
					.append(member.getOmMail())
					.append("&authkey=")
					.append(authkey)
					.append("' target='_blenk'>이메일 인증 확인</a>")
					.toString());
		} catch (javax.mail.MessagingException e) {
			e.printStackTrace();
		}
		sender.send(message);
		
		
		// 프로파일 업로드 , 프로파일 업로드안했을시 예외처리 안해줌
//		MultipartFile omProfileFile = member.getOmProfileFile();
//		String fileName = omProfileFile.getOriginalFilename();
//		String extName = FilenameUtils.getExtension(fileName);
//		String reName = Long.toString(System.nanoTime()); //원레이름
//		reName += "." +extName; //최종이름
//		File targetFile = new File(BASE+reName);
//		try {
//			Files.copy(omProfileFile.getInputStream(), targetFile.toPath());
//
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		member.setOmProfile(reName);

		return mm.insertMember(member);
	}

	
	// 회원가입 메일인증 받으면 1
	@Override
	public int joinUpdate(MemberVO member) {
		member.setAuthstatus(1);
		return mm.joinUpdate(member);
	}
	
	
	@Override
	public int updateMember(MemberVO member) {
		MultipartFile omProfileFile = member.getOmProfileFile();

		String fileName = omProfileFile.getOriginalFilename();
		String extName = FilenameUtils.getExtension(fileName);
		String reName = Long.toString(System.nanoTime()); // 원레이름
		reName += "." + extName; // 최종이름
		File targetFile = new File(BASE + reName);
		try {
			Files.copy(omProfileFile.getInputStream(), targetFile.toPath());
		} catch (IOException e) {
			e.printStackTrace();
		} // ---- 올린파일 저장하기위한 로직

		member.setOmProfile(reName); // 올린파일로 db에 저장
		MemberVO orgMember = mm.selectMemberById(member.getOmId()); // 전에있는 데이터를 조회
		if (mm.updateMember(member) == 1) { // 새로운걸 업데이트 1이 되었을경우 업데이트(정상적으로)
			if (orgMember.getOmProfile() != null) {
				File orgFile = new File(BASE + orgMember.getOmProfile()); // 프로파일 가져와서
				if (orgFile.exists()) {
					orgFile.delete(); // 전에있던 프로파일 삭제
				}
			}
			return 1;
		} else {
			targetFile.delete(); // 업데이트가 1이 안되었을 경우
		}

		return mm.updateMember(member);
	}

	@Override
	public MemberVO selectMemberById(String omId) {
		return mm.selectMemberById(omId);
	}

	@Override
	public MemberVO selectMemberByIdAndPwd(MemberVO member) {
//		member.setOmPass(SHAEncoder.encode(member.getOmPass()));  // 클래스 사용한 방법
		member.setOmPass(DigestUtils.sha256Hex(member.getOmPass()));  // 라이브러리 사용한 방법
//		log.info("SERVICE !!!=>before member{}",member);
		MemberVO returnMember = mm.selectMemberByIdAndPwd(member);
//		log.info("returnMember !!!!! =>{}",returnMember);
		if (returnMember.getOmPass() != null) {
			returnMember.setOmToken(MakeJWT.makeJWT(member));
		}
		return returnMember;

//		return mm.selectMemberByIdAndPwd(member);
	}

	@Override
	public int deleteMember(int omNo) {
		return mm.deleteMember(omNo);
	}

	//아이디 찾기
	@Override
	public MemberVO selectIdFind(MemberVO member) {
		
		// 메일 인증키 
		String authkey = new TempKey().getKey(50, false);
		member.setAuthkey(authkey);
		
		MemberVO res = mm.selectIdFind(member);

				if(member.getOmMail()== res.getOmMail()) {
					// 메일보내기
					MimeMessage message = sender.createMimeMessage();
					MimeMessageHelper helper = new MimeMessageHelper(message);
					try {
						helper.setTo(member.getOmMail());
						helper.setSubject("[페이스톡톡] 아이디 입니다");
						helper.setText(new StringBuffer().append("아이디는")
								.append(member.getOmId())
								.append("입니다.")
								.toString());
					} catch (javax.mail.MessagingException e) {
						e.printStackTrace();
					}
					sender.send(message);

				}
				log.info("email  ==> {}", member.getOmMail());

			
		return mm.selectIdFind(member);
	}

	

}
