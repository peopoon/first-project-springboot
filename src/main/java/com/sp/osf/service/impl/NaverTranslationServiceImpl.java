package com.sp.osf.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.sp.osf.mapper.NaverTranslationMapper;
import com.sp.osf.service.NaverTranslationService;
@Service
public class NaverTranslationServiceImpl implements NaverTranslationService {

	@Resource
	private NaverTranslationMapper ntm;
	
	@Override
	public List<Map<String, Object>> selectTranslationHisList() {
		return ntm.selectTranslationHisList();
	}

	@Override
	public Map<String, Object> selectTranslationHisOne(Map<String, String> param) {
		return ntm.selectTranslationHisOne(param);
	}

	@Override
	public Integer insertTranslationHis(Map<String, String> param) {
		return ntm.insertTranslationHis(param);
	}

	@Override
	public Integer updateTranslationHis(Map<String, Object> param) {
		return ntm.updateTranslationHis(param);
	}

	

}
