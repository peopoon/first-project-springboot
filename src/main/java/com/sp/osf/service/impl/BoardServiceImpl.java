//package com.sp.osf.service.impl;
//
//import javax.annotation.Resource;
//
//import org.springframework.stereotype.Service;
//
//import com.sp.osf.mapper.BoardMapper;
//import com.sp.osf.service.BoardService;
//import com.sp.osf.vo.BoardVO;
//import com.sp.osf.vo.CommentsVO;
//import com.sp.osf.vo.PageDTO;
//@Service
//public class BoardServiceImpl implements BoardService {
//	
//	@Resource
//	private BoardMapper boardMapper;
//	
//	@Override
//	public int write(BoardVO boardVO) {
//		if(boardVO.getGNo()==0) {
//			return boardMapper.write(boardVO);
//		}else {
//			boardMapper.replyShape(boardVO);
//		}
//		return boardMapper.writeReply(boardVO);
//	}
//
//	@Override
//	public BoardVO view(int no) {
//		return boardMapper.view(no);
//	}
//
//	@Override
//	public java.util.List<BoardVO> List(PageDTO page) {
//		page.setStartRow((page.getPageNum()-1 ) * page.getPageSize());
//		page.setEndRow(page.getStartRow()+ page.getPageSize() -1);
//		return boardMapper.List(page);
//	}
//
//	@Override
//	public int getCount() {
//		return boardMapper.getCount();
//	}
//
//	@Override
//	public int writeComments(CommentsVO commentsVO) {
//		return boardMapper.writeComments(commentsVO);
//	}
//
//	@Override
//	public java.util.List<CommentsVO> listComments(int bNo) {
//		return boardMapper.listComments(bNo);
//	}
//
//}
