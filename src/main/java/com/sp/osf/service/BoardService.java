package com.sp.osf.service;

import java.util.List;

import com.sp.osf.vo.BoardVO;
import com.sp.osf.vo.CommentsVO;
import com.sp.osf.vo.PageDTO;

public interface BoardService {

	int write(BoardVO boardVO);
	
	BoardVO view(int no);
	
	List<BoardVO> List(PageDTO page);
	
	int getCount();
	
	int writeComments(CommentsVO commentsVO);
	
	List<CommentsVO> listComments(int bNo);
	
}
