package com.sp.osf.service;

import java.util.List;

import com.sp.osf.vo.MemberVO;

public interface MemberService {
	
	// 회원들 리스트
	public List<MemberVO> selectMemberList(MemberVO member);
	// 회원 조회
	public MemberVO selectMember(int omNo);
	// 로그인
	public MemberVO selectMemberByIdAndPwd(MemberVO member);
	// 회원가입
	int insertMember(MemberVO member);
	// 회원 정보 변경
	int updateMember(MemberVO member);
	// 회원 삭제
	int deleteMember(int omNo);
	// 회원 데이터 ID로 가져오기
	public MemberVO selectMemberById(String omId);
	

	// 회원가입 메일인증 받으면 1
	int joinUpdate(MemberVO member);
	// 아이디 찾기
	public MemberVO selectIdFind(MemberVO member);
	
	
}
