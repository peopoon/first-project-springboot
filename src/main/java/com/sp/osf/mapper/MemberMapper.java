package com.sp.osf.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.sp.osf.vo.MemberVO;
@Mapper
public interface MemberMapper {
	public List<MemberVO> selectMemberList(MemberVO member);
	public MemberVO selectMember(int omNo);
	public MemberVO selectMemberById(String omId);
	public MemberVO selectMemberByIdAndPwd(MemberVO member);

	
	int insertMember(MemberVO member);
	int updateMember(MemberVO member);
	int deleteMember(int omNo);
	
	// 회원가입 메일인증 받으면 1
	int joinUpdate(MemberVO member);
	// 아이디 찾기
	public MemberVO selectIdFind(MemberVO member);
	
}
