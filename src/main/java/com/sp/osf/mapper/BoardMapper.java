package com.sp.osf.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.sp.osf.vo.BoardVO;
import com.sp.osf.vo.CommentsVO;
import com.sp.osf.vo.PageDTO;
@Mapper
public interface BoardMapper {
	int write(BoardVO boardVO);
	BoardVO view(int no);
	List<BoardVO> List(PageDTO page);
	
	int getCount();
	
	int writeComments(CommentsVO commentsVO);
	
	List<CommentsVO> listComments(int bNo);
	
}
