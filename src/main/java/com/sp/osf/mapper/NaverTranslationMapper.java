package com.sp.osf.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
@Mapper
public interface NaverTranslationMapper {
	public List<Map<String, Object>> selectTranslationHisList();
	
	public Map<String, Object> selectTranslationHisOne(Map<String, String> param);
	public Integer insertTranslationHis(Map<String, String> param);
	public Integer updateTranslationHis(Map<String, Object> param);

}
