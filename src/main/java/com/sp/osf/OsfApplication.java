package com.sp.osf;

import javax.sql.DataSource;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
@EnableAutoConfiguration(exclude= {DataSourceAutoConfiguration.class})
public class OsfApplication {

	public static void main(String[] args) {
		SpringApplication.run(OsfApplication.class, args);
	}
	
	@Bean 
	@ConfigurationProperties(prefix="spring.datasource.hikari")
	public DataSource getDataSource() {
		return DataSourceBuilder.create().build();
	}
	

}
